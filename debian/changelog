python-fisx (1.3.2-1) unstable; urgency=medium

  * Upload into unstable
  * Bug fix: "Fails to build source after successful build", thanks to
    Lucas Nussbaum (Closes: #1047724).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 03 Mar 2025 14:30:51 +0100

python-fisx (1.3.2-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/control: Require numpy 2.0, only available in Experimental now.

 -- Boyuan Yang <byang@debian.org>  Fri, 11 Oct 2024 11:53:55 -0400

python-fisx (1.3.1-1) unstable; urgency=medium

  * New upstream version 1.3.1
  * d/control: Added B-D pybuild-plugin-pyproject

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 23 Jun 2023 10:04:19 +0200

python-fisx (1.2.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python3-fisx-dbg: Add Multi-Arch: same.
    + python-fisx-common: Add Multi-Arch: foreign.
  * Bump debhelper from old 12 to 13.

  [ Picca Frédéric-Emmanuel ]
  * Bug fix: "Removal of the python3-*-dbg packages in sid/bookworm",
    thanks to Matthias Klose (Closes: #994352).
  * d/control: Used dh-sequence-[numpy3,python3]
  * d/rules: Updated with execute_after_X

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 19 Sep 2021 11:14:46 +0200

python-fisx (1.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 1.2.0

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 06 Jan 2021 14:21:03 +0100

python-fisx (1.1.8-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 1.1.8

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 10 Apr 2020 10:19:16 +0200

python-fisx (1.1.7-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Andrey Rahmatullin ]
  * Drop Python 2 support (Closes: #937754).

 -- Andrey Rahmatullin <wrar@debian.org>  Mon, 14 Oct 2019 21:45:00 +0500

python-fisx (1.1.7-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Rename d/tests/control.autodep8 to d/tests/control.

  [ Alexandre Marie ]
  * New upstream version 1.1.7
  * d/control: Added myself to uploaders
  * d/copyright: Changed order of files.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 09 Jul 2019 12:17:28 +0200

python-fisx (1.1.6-1) unstable; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 1.1.6
  * d/tests
    - Moved all tests into control.autodep8

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 07 Sep 2018 10:43:46 +0200

python-fisx (1.1.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Remove Testsuite field, not needed anymore
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 1.1.5 (Closes: #904263)
  * make the package build reproducible (Closes: #894466)
    Thanks Chris Lamb for thr patch.

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 02 Aug 2018 15:43:21 +0200

python-fisx (1.1.4-1) unstable; urgency=medium

  * New upstream version 1.1.4
  * Fix autopkgtest dependencies (Closes: #864697)
  * debian/watch use special strings
  * Bumped Standards-Version to 4.1.1 (nothing to do)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 25 Nov 2017 13:46:47 +0100

python-fisx (1.1.2-1) unstable; urgency=medium

  * New upstream version 1.1.2

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 27 Nov 2016 10:28:10 +0100

python-fisx (1.1.1-1) unstable; urgency=medium

  * Imported Upstream version 1.1.1

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 17 Aug 2016 21:15:00 +0200

python-fisx (1.0.8-1) unstable; urgency=low

  * Initial package (Closes: #827609)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 09 Jun 2016 10:17:18 +0200
